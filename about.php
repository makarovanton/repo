<!DOCTYPE>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
    	
        <h1>Краткая информация</h1>
<?php
$name = "Антон";
$age = 23;
$mail = "makarovanton2016@yandex.ru";
$city = "Тула";
$profession = "Инженер-программист";
?>
        <dl>
            <dt>Имя</dt>
            <dd><?php echo $name ?></dd>
        </dl>
        <dl>
            <dt>Возраст</dt>
            <dd><?php echo $age ?></dd>
        </dl>
        <dl>
            <dt>Адрес электронной почты</dt>
            <dd><?php echo $mail ?></dd>
        </dl>
        <dl>
            <dt>Город</dt>
            <dd><?php echo $city ?></dd>
        </dl>
        <dl>
            <dt>О себе</dt>
            <dd><?php echo $profession ?></dd>
        </dl>
    </body>
</html>
